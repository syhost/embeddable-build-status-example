# embeddable-build-status-example

[![Build Status](http://jenkins.voicetree.info:62126/buildStatus/icon?job=API+Pipeline)](http://jenkins.voicetree.info:62126/buildStatus/icon?job=API+Pipeline)
[![Build Status](https://jenkins.myoperator.co/buildStatus/icon?job=API+Pipeline)](https://jenkins.myoperator.co/buildStatus/icon?job=API+Pipeline)
[![pipeline status](https://gitlab.com/ashutosh.myoperator/embeddable-build-status-example/badges/master/pipeline.svg)](https://gitlab.com/ashutosh.myoperator/embeddable-build-status-example/commits/master)


This example shows how embeddable-build-status badge isn't able to render on gitlab but can render on github and other providers.